#! /usr/bin/zsh

curl "https://www.dwd.de/DE/leistungen/met_verfahren_mosmix/mosmix_stationskatalog.cfg?view=nasPublication&nn=16102" | iconv -f latin1 -t utf-8 > stations.cfg.utf8
curl "https://opendata.dwd.de/weather/local_forecasts/mos/MOSMIX_L/single_stations/10156/kml/MOSMIX_L_LATEST_10156.kmz" | funzip > 10156.kml
curl "https://opendata.dwd.de/weather/local_forecasts/mos/MOSMIX_L/all_stations/kml/MOSMIX_L_LATEST.kmz" | funzip > dwd_data.kml

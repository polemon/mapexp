# mapexp

Creating my own version of a weather forecast map using OSM maps and weather data from the DWD (Deutscher Wetterdienst)

## Getting started

First the kml data should be downloaded using `getkml.sh`, the `dwd_data.kml` is pretty large (around 2GB),
hence it shouldn't be checked into git.

`kml` files can be previewed with software like GPXsee.

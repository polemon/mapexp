var blueIcon = new L.Icon({
    iconUrl: 'leaflet-color-markers/img/blue.png',
    shadowUrl: 'leaflet-color-markers/img/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var redIcon = new L.Icon({
    iconUrl: 'leaflet-color-markers/img/red.png',
    shadowUrl: 'leaflet-color-markers/img/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var greenIcon = new L.Icon({
    iconUrl: 'leaflet-color-markers/img/green.png',
    shadowUrl: 'leaflet-color-markers/img/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var orangeIcon = new L.Icon({
    iconUrl: 'leaflet-color-markers/img/orange.png',
    shadowUrl: 'leaflet-color-markers/img/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var yellowIcon = new L.Icon({
    iconUrl: 'leaflet-color-markers/img/yellow.png',
    shadowUrl: 'leaflet-color-markers/img/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var violetIcon = new L.Icon({
    iconUrl: 'leaflet-color-markers/img/violet.png',
    shadowUrl: 'leaflet-color-markers/img/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var greyIcon = new L.Icon({
    iconUrl: 'leaflet-color-markers/img/grey.png',
    shadowUrl: 'leaflet-color-markers/img/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var darkgreyIcon = new L.Icon({
    iconUrl: 'leaflet-color-markers/img/darkgrey.png',
    shadowUrl: 'leaflet-color-markers/img/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var lightgreyIcon = new L.Icon({
    iconUrl: 'leaflet-color-markers/img/lightgrey.png',
    shadowUrl: 'leaflet-color-markers/img/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var pinkIcon = new L.Icon({
    iconUrl: 'leaflet-color-markers/img/pink.png',
    shadowUrl: 'leaflet-color-markers/img/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

#!/usr/bin/env python3

# get station catalog from DWD
# last change: 2021-07-09
# v0.01

import requests
import re
from datetime import datetime
import json

DWD_URL = "https://www.dwd.de/DE/leistungen/met_verfahren_mosmix/mosmix_stationskatalog.cfg"
DWD_PARAMS = { "view": "nasPublication", "nn": 16102 }
DWD_HEADERS = { "User-Agent": "python_requests/7.76.1", "Accept": "*/*"} # this is important, because if it's: "python-requests" the server breaks! ¦D

# convert the weird CFG format into regular decimal degrees.
# The decimal format in the CFG format isn't a decimal point, it's a mere separator between
# degrees and arc minutes.
def degminstrtodeg(degminStr):
    degStr, minStr = degminStr.split('.')
    return int(degStr) + (int(minStr) * (1/60))

def todict(line):
    # format is really quite bad.
    # it is based on fixed lengths in this pattern:
    #   clu   CofX  id    ICAO name                 nb.    el.     elev  Hmod-H type
    #   ===== ----- ===== ---- -------------------- ------ ------- ----- ------ ----
    #   99803     8 EW002 ---- Beveringen            53.10   12.13    71        LAND
    # 
    # Every line is 76 characters long, delimiters are spaces, however spaces are also used for padding.
    #
    # clu:      5 characters; if a negative digit is used it's only four digits with a '-' infront of the number.
    #           seems to be left-aligned but all numbers fill all five characters and possibly it's zero-padded.
    # CofX:     5 characters; right-aligned and sometimes empty (filled with spaces). Padded with spaces.
    # id:       5 characters; alphanumeric and left-aligned. Seems to be a unique ID. Padded with spaces.
    # ICAO:     4 characters; filled with the ICAO code when it's associated with an airport or airfield.
    #           if empty filled with '----'.
    # name:     20 characters; left-aligned. Often in ALL CAPS. Padded with spaces. Never omitted, but NOT unique!
    # nb.:      6 characters; right-aligned. Negative for southern hemisphere. Padded with spaces.
    #           The format isn't a decimal number, even though it uses a period as spacer.
    #           A number like: "53.12" is NOT 53.12 degrees north, rather it's 53°12´ north,
    #           i.e.: "53 degrees, 12 minutes".
    # el.       7 characters; right-aligned. Negative for western hemisphere. Padded with spaces.
    #           Format is as confusing as for "nb" - see that for description.
    # elev:     5 characters; right-aligned. SOMETIMES NEGATIVE! '0' if zero, never omitted. Padded with spaces.
    # Hmod-H:   6 characters; right-aligned, Sometimes empty (filled with spaces). Sometimes negative. Padded with spaces.
    # type:     4 characters; one of four codes: "MEER" (sea), "KUES" (coast), "LAND" (inland), or "BERG" (mountain).
    #           sometimes omitted / empty!

    #print(line)

    raw_data = {}

    raw_data["clu"]    = line[0:5]
    raw_data["CofX"]   = line[6:11]
    raw_data["id"]     = line[12:17]
    raw_data["ICAO"]   = line[18:22]
    raw_data["name"]   = line[23:43]
    raw_data["nb."]    = line[44:50]
    raw_data["el."]    = line[51:58]
    raw_data["elev"]   = line[59:64]
    raw_data["Hmod-H"] = line[65:71]
    raw_data["type"]   = line[72:76]
    
    data = {}

    data["clu"] = int(raw_data["clu"])

    try:
        data["CofX"] = int(raw_data["CofX"])
    except ValueError:
        pass

    data["id"] = raw_data["id"].strip()

    if not raw_data["ICAO"] == "----":
        data["ICAO"] = raw_data["ICAO"]

    data["name"] = raw_data["name"].strip()

    data["latitude"] = degminstrtodeg(raw_data["nb."].strip())
    data["longitude"] = degminstrtodeg(raw_data["el."].strip())

    data["elevation"] = int(raw_data["elev"].strip())

    try:
        data["Hmod-H"] = int(raw_data["Hmod-H"])
    except ValueError:
        pass

    data["type"] = raw_data["type"]

    return data


def main():
    dwd_response = requests.get(DWD_URL, params = DWD_PARAMS, headers = DWD_HEADERS)
    
    if not dwd_response.status_code == requests.codes.ok:
        print("something went wrong")
        print(f"error: {dwd_response.status_code}")
        exit()

    # print(dwd_response.status_code)
    cfg = dwd_response.content.decode('latin1')
    reg = re.compile('^-?\d+')

    stations = {}
    for i in cfg.splitlines():
        if reg.match(i):
            station = todict(i)
            key = station.pop("id")
            stations[key] = station

    ts = int(datetime.now().timestamp())
    d = datetime.utcfromtimestamp(ts)

    timestamp = {}
    timestamp["epoch"] = ts
    timestamp["datetime"] = { "year": d.year, "month": d.month, "day": d.day, "hour": d.hour, "minutes": d.minute, "second": d.second }

    dwd_catalog = {}
    dwd_catalog["timestamp"] = timestamp
    dwd_catalog["stations"] = stations

    with open("dwd_catalog.json", 'w') as F:
        json.dump(dwd_catalog, F, ensure_ascii = False, indent = 2)

if __name__ == "__main__":
    main()
